﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Vokabeltrainer
{
    public partial class Form_Settings : Form
    {
        public Form_Settings()
        {
            InitializeComponent();
            cBox1_Mode.Items.Add("Deutsch/Englisch");
            cBox1_Mode.Items.Add("Englisch/Deutsch");
            cBox1_Mode.SelectedIndex = Options.GetInstance().TranslateMode;                  
            nUD1_vocCount.Value = Options.GetInstance().VocabCount;
            tB1_note1.Text = Options.GetInstance()[0];
            nUD2_note1Percent.Value = (decimal)Options.GetInstance()["0"];
            tB2_note2.Text = Options.GetInstance()[1];
            nUD3_note2Percent.Value = (decimal)Options.GetInstance()["1"];
            tB3_note3.Text = Options.GetInstance()[2];
            nUD4_note3Percent.Value = (decimal)Options.GetInstance()["2"];
            tB4_note4.Text = Options.GetInstance()[3];
            nUD5_note4Percent.Value = (decimal)Options.GetInstance()["3"];
            tB5_note5.Text = Options.GetInstance()[4];

        }
        
        private void cBox1_Mode_SelectedIndexChanged(object sender, EventArgs e)
        {
            Options.GetInstance().TranslateMode = cBox1_Mode.SelectedIndex;
        }

        private void nUD1_vocCount_ValueChanged(object sender, EventArgs e)
        {
            Options.GetInstance().VocabCount = Convert.ToInt32(nUD1_vocCount.Value);
        }
            
        #region marks
        private void tB1_note1_TextChanged(object sender, EventArgs e)
        {
            Options.GetInstance()[0] = tB1_note1.Text;
        }

        private void nUD2_note1Percent_ValueChanged(object sender, EventArgs e)
        {
            Options.GetInstance()["0"] = (double)nUD2_note1Percent.Value;
        }

        private void tB2_note2_TextChanged(object sender, EventArgs e)
        {
            Options.GetInstance()[1] = tB2_note2.Text;
        }

        private void nUD3_note2Percent_ValueChanged(object sender, EventArgs e)
        {
            Options.GetInstance()["1"] = (double)nUD3_note2Percent.Value;
        }

        private void tB3_note3_TextChanged(object sender, EventArgs e)
        {
            Options.GetInstance()[2] = tB3_note3.Text;
        }

        private void nUD4_note3Percent_ValueChanged(object sender, EventArgs e)
        {
            Options.GetInstance()["2"] = (double)nUD4_note3Percent.Value;
        }

        private void tB4_note4_TextChanged(object sender, EventArgs e)
        {
            Options.GetInstance()[3] = tB4_note4.Text;
        }

        private void nUD5_note4Percent_ValueChanged(object sender, EventArgs e)
        {
            Options.GetInstance()["3"] = (double)nUD5_note4Percent.Value;
        }

        private void tB5_note5_TextChanged(object sender, EventArgs e)
        {
            Options.GetInstance()[4] = tB5_note5.Text;
        }
        #endregion

        private void bt1_save_Click(object sender, EventArgs e)
        {
            Options.GetInstance().WriteOptions();
        }
    }
}
