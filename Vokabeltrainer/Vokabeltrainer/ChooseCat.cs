﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Vokabeltrainer
{
    public partial class ChooseCat : Form
    {
        int ccat;

        public ChooseCat()
        {
            InitializeComponent();
        }
        
        private void ChooseCat_Load(object sender, EventArgs e)
        {
            DatabaseConnection.GetInstance().Open();
            FillcbCCat(DatabaseConnection.GetInstance().Con);
        }

        public int CCat
        {
            get { return ccat; }
            set { ccat = value; }
        }

        private void FillcbCCat(OleDbConnection con)
        {
            string sql = "select ID, Beschreibung from Kategorie";
            OleDbCommand cmd = new OleDbCommand(sql, con);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cbCCat.Items.Add(new Category(reader.GetInt32(0), reader.GetString(1)));
                cbCCat.SelectedIndex = 0;
            }
            reader.Close();
        }

        private void cbCCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            CCat = ((Category)cbCCat.SelectedItem).ID;
        }

        private void b1_CCat_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
