﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vokabeltrainer
{
    class Category
    {

        int id;
        string desc;

        public Category(int id, string desc)
        {
            ID = id;
            Desc = desc;
        }

        public override string ToString()
        {
            return ID + " - " + Desc;
        }

        public int ID
        {
            get { return id; }
            set
            {
                id = value;
            }
        }

        public string Desc
        {
            get { return desc; }
            set
            {
                desc = value;
            }
        }

    }
}
