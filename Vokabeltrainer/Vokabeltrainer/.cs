﻿
namespace Vokabeltrainer
{
    partial class Form_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Settings));
            this.bt1_save = new System.Windows.Forms.Button();
            this.bt2_cancel = new System.Windows.Forms.Button();
            this.cBox1_Mode = new System.Windows.Forms.ComboBox();
            this.nUD1_vocCount = new System.Windows.Forms.NumericUpDown();
            this.label1_count = new System.Windows.Forms.Label();
            this.label2_lang = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nUD1_vocCount)).BeginInit();
            this.SuspendLayout();
            // 
            // bt1_save
            // 
            this.bt1_save.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bt1_save.Location = new System.Drawing.Point(35, 207);
            this.bt1_save.Name = "bt1_save";
            this.bt1_save.Size = new System.Drawing.Size(75, 23);
            this.bt1_save.TabIndex = 0;
            this.bt1_save.Text = "Speichern";
            this.bt1_save.UseVisualStyleBackColor = true;
            this.bt1_save.Click += new System.EventHandler(this.bt1_save_Click);
            // 
            // bt2_cancel
            // 
            this.bt2_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt2_cancel.Location = new System.Drawing.Point(174, 207);
            this.bt2_cancel.Name = "bt2_cancel";
            this.bt2_cancel.Size = new System.Drawing.Size(75, 23);
            this.bt2_cancel.TabIndex = 1;
            this.bt2_cancel.Text = "Abbrechen";
            this.bt2_cancel.UseVisualStyleBackColor = true;
            // 
            // cBox1_Mode
            // 
            this.cBox1_Mode.FormattingEnabled = true;
            this.cBox1_Mode.Location = new System.Drawing.Point(129, 122);
            this.cBox1_Mode.Name = "cBox1_Mode";
            this.cBox1_Mode.Size = new System.Drawing.Size(121, 21);
            this.cBox1_Mode.TabIndex = 2;
            this.cBox1_Mode.SelectedIndexChanged += new System.EventHandler(this.cBox1_Mode_SelectedIndexChanged);
            // 
            // nUD1_vocCount
            // 
            this.nUD1_vocCount.Location = new System.Drawing.Point(129, 58);
            this.nUD1_vocCount.Name = "nUD1_vocCount";
            this.nUD1_vocCount.Size = new System.Drawing.Size(120, 20);
            this.nUD1_vocCount.TabIndex = 3;
            this.nUD1_vocCount.ValueChanged += new System.EventHandler(this.nUD1_vocCount_ValueChanged);
            // 
            // label1_count
            // 
            this.label1_count.AutoSize = true;
            this.label1_count.Location = new System.Drawing.Point(13, 58);
            this.label1_count.Name = "label1_count";
            this.label1_count.Size = new System.Drawing.Size(77, 13);
            this.label1_count.TabIndex = 4;
            this.label1_count.Text = "Vokabelanzahl";
            // 
            // label2_lang
            // 
            this.label2_lang.AutoSize = true;
            this.label2_lang.Location = new System.Drawing.Point(13, 122);
            this.label2_lang.Name = "label2_lang";
            this.label2_lang.Size = new System.Drawing.Size(80, 13);
            this.label2_lang.TabIndex = 5;
            this.label2_lang.Text = "Sprachauswahl";
            // 
            // Form_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label2_lang);
            this.Controls.Add(this.label1_count);
            this.Controls.Add(this.nUD1_vocCount);
            this.Controls.Add(this.cBox1_Mode);
            this.Controls.Add(this.bt2_cancel);
            this.Controls.Add(this.bt1_save);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_Settings";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Einstellungen";
            ((System.ComponentModel.ISupportInitialize)(this.nUD1_vocCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt1_save;
        private System.Windows.Forms.Button bt2_cancel;
        private System.Windows.Forms.ComboBox cBox1_Mode;
        private System.Windows.Forms.NumericUpDown nUD1_vocCount;
        private System.Windows.Forms.Label label1_count;
        private System.Windows.Forms.Label label2_lang;
    }
}