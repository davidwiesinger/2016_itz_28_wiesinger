﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace Vokabeltrainer
{
    partial class Form1 : Form
    {
        Panel pan_back = null;
        int ccat;
        public Form1()
        {
            InitializeComponent();
            
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            DatabaseConnection.GetInstance().Open();
        }      

        private void CreateEntries() 
        {
            pan_back = new Panel();
            TextBox tx_vokabel;
            Label lb_vokabel;
            
            int top_lab = 20; int top_text = 20; int topAdder = 40;
            int left_lab = 20; int left_text = 175;

            pan_back.Top = 50;
            pan_back.Left = 60;
            pan_back.Height = 350;
            pan_back.Width = 350;
            pan_back.BorderStyle = BorderStyle.FixedSingle;
            this.Controls.Add(pan_back);

            if (Vokabeltrainer.GetInstance().Vokabeln.Count * topAdder > pan_back.Height)
                pan_back.AutoScroll = true;
            else
                pan_back.AutoScroll = false;

            for (int i = 0; i < ((Vokabeltrainer.GetInstance().Vokabeln.Count < Options.GetInstance().VocabCount)? Vokabeltrainer.GetInstance().Vokabeln.Count : Options.GetInstance().VocabCount); i++)
            {         
                top_lab += ((i != 0)?topAdder:0);
                lb_vokabel = new Label();
                lb_vokabel.AutoSize = true;
                lb_vokabel.Top = top_lab;
                lb_vokabel.Left = left_lab;
                lb_vokabel.Text = (Options.GetInstance().TranslateMode == 0)? Vokabeltrainer.GetInstance().Vokabeln[i].German: Vokabeltrainer.GetInstance().Vokabeln[i].English;
                pan_back.Controls.Add(lb_vokabel);
                
                top_text += ((i != 0)?topAdder: 0);
                tx_vokabel = new TextBox();
                lb_vokabel.AutoSize = true;
                tx_vokabel.Left = left_text;
                tx_vokabel.Top = top_text;
                tx_vokabel.Tag = Vokabeltrainer.GetInstance().Vokabeln[i];
                pan_back.Controls.Add(tx_vokabel);
            }
        }

        private void bt2_Reload_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void bt1_Check_Click(object sender, EventArgs e)
        {
            List<Vokabel> vl = new List<Vokabel>();
            List<string> stl = new List<string>();
            //Eventregestrierung
            Vokabeltrainer.GetInstance().VocabCheck += Form1_VocabCheck;
            Vokabeltrainer.GetInstance().Note += Form1_Note;
            if (pan_back != null)
            {


                foreach (Control cont in this.pan_back.Controls)
                {
                    if (cont is TextBox)
                    {
                        Vokabel v = (Vokabel)cont.Tag;
                        TextBox tx = (TextBox)cont;
                        vl.Add(v);
                        stl.Add(tx.Text);
                    }
                }
                Vokabeltrainer.GetInstance().CheckVocab(vl, stl);
            }         
        }      

        private void F1_vktrainer_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (pan_back != null)
            {               
                if (MessageBox.Show("Wollen sie wirklich abbrechen?", "Beenden", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    e.Cancel = false;
                else
                    e.Cancel = true;
            }
            DatabaseConnection.GetInstance().Close();
        }

        private void Reload()
        {
                if (this.Controls.Contains(pan_back))
                {
                    this.Controls.Remove(pan_back);
                    pan_back = null;
                }
                Vokabeltrainer.GetInstance().SortEntries();
                CreateEntries();
        }

        private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
        { 
                Application.Exit();
        }

        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Settings fst = new Form_Settings();
            fst.StartPosition = FormStartPosition.CenterParent;
            DialogResult res = fst.ShowDialog();
            if (res == DialogResult.OK)
            {
                Options.GetInstance().WriteOptions();
            }
        
        }

        private void Form1_Note(string note)
        {
            MessageBox.Show(note, "Beurteilung");
        }

        private void Form1_VocabCheck(bool ifRight, Vokabel voc)
        {
            if (ifRight) //green if it is correct
            {
                foreach (Control cont in this.pan_back.Controls)
                {
                    if (cont is TextBox)
                    {
                        Vokabel v = (Vokabel)cont.Tag;
                        if (Options.GetInstance().TranslateMode == 0)
                        {
                            if (v.English == voc.English)
                                cont.BackColor = Color.Green;
                        }
                        else
                        {
                            if (v.German == voc.German)
                                cont.BackColor = Color.Green;
                        }
                    }
                }
            }
            else
            {
                foreach (Control cont in this.pan_back.Controls)
                {
                    if (cont is TextBox)
                    {
                        Vokabel v = (Vokabel)cont.Tag;
                        if (Options.GetInstance().TranslateMode == 0)
                        {
                            if (v.English == voc.English)
                                cont.BackColor = Color.Red;
                        }
                        else
                        {
                            if (v.German == voc.German)
                                cont.BackColor = Color.Red;
                        }
                    }
                }
            }
        }

        private void OpenChooseCat()
        {
            ChooseCat cc1 = new ChooseCat();
            cc1.ShowDialog();
            ccat = cc1.CCat;
            Vokabeltrainer.GetInstance().ReadFromDB(DatabaseConnection.GetInstance().Con, ccat);
            Reload();
        }

        private void neuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenChooseCat();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            OpenChooseCat();
        }

        private void kategorienToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Category_Form cf1 = new Category_Form();
            cf1.ShowDialog();
        }
    }
}
