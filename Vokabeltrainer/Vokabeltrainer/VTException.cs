﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Vokabeltrainer
{
    public class VTException : Exception
    {
        public VTException()
        {

        }
        public VTException(string message) : base(message)
        {

        }
        public VTException(string message, Exception inner) : base(message, inner)
        {
            Logger(inner);
        }

        private void Logger(Exception ex)
        {
            StreamWriter sw = new StreamWriter("vt_errors.log", true);

            sw.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " " + ex.Message);

            sw.Flush();
            sw.Close();
        }
    }
}
