﻿namespace Vokabeltrainer
{
    partial class Category_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCatE = new System.Windows.Forms.ListBox();
            this.tbCatEName = new System.Windows.Forms.TextBox();
            this.laCatEName = new System.Windows.Forms.Label();
            this.bCatENeu = new System.Windows.Forms.Button();
            this.bCatEDelete = new System.Windows.Forms.Button();
            this.bCatESave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbCatE
            // 
            this.lbCatE.FormattingEnabled = true;
            this.lbCatE.ItemHeight = 16;
            this.lbCatE.Location = new System.Drawing.Point(12, 13);
            this.lbCatE.Name = "lbCatE";
            this.lbCatE.Size = new System.Drawing.Size(282, 228);
            this.lbCatE.TabIndex = 0;
            this.lbCatE.SelectedIndexChanged += new System.EventHandler(this.lbCatE_SelectedIndexChanged);
            // 
            // tbCatEName
            // 
            this.tbCatEName.Enabled = false;
            this.tbCatEName.Location = new System.Drawing.Point(63, 245);
            this.tbCatEName.Name = "tbCatEName";
            this.tbCatEName.Size = new System.Drawing.Size(231, 22);
            this.tbCatEName.TabIndex = 1;
            // 
            // laCatEName
            // 
            this.laCatEName.AutoSize = true;
            this.laCatEName.Location = new System.Drawing.Point(12, 248);
            this.laCatEName.Name = "laCatEName";
            this.laCatEName.Size = new System.Drawing.Size(45, 17);
            this.laCatEName.TabIndex = 2;
            this.laCatEName.Text = "Name";
            // 
            // bCatENeu
            // 
            this.bCatENeu.Location = new System.Drawing.Point(12, 279);
            this.bCatENeu.Name = "bCatENeu";
            this.bCatENeu.Size = new System.Drawing.Size(90, 23);
            this.bCatENeu.TabIndex = 3;
            this.bCatENeu.Text = "Neu";
            this.bCatENeu.UseVisualStyleBackColor = true;
            this.bCatENeu.Click += new System.EventHandler(this.bCatENeu_Click);
            // 
            // bCatEDelete
            // 
            this.bCatEDelete.Enabled = false;
            this.bCatEDelete.Location = new System.Drawing.Point(204, 279);
            this.bCatEDelete.Name = "bCatEDelete";
            this.bCatEDelete.Size = new System.Drawing.Size(90, 23);
            this.bCatEDelete.TabIndex = 4;
            this.bCatEDelete.Text = "Löschen";
            this.bCatEDelete.UseVisualStyleBackColor = true;
            this.bCatEDelete.Click += new System.EventHandler(this.bCatEDelete_Click);
            // 
            // bCatESave
            // 
            this.bCatESave.Location = new System.Drawing.Point(108, 279);
            this.bCatESave.Name = "bCatESave";
            this.bCatESave.Size = new System.Drawing.Size(90, 23);
            this.bCatESave.TabIndex = 5;
            this.bCatESave.Text = "Speichern";
            this.bCatESave.UseVisualStyleBackColor = true;
            this.bCatESave.Click += new System.EventHandler(this.bCatESave_Click);
            // 
            // Category_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 314);
            this.Controls.Add(this.bCatESave);
            this.Controls.Add(this.bCatEDelete);
            this.Controls.Add(this.bCatENeu);
            this.Controls.Add(this.laCatEName);
            this.Controls.Add(this.tbCatEName);
            this.Controls.Add(this.lbCatE);
            this.Name = "Category_Form";
            this.Text = "Kategorien";
            this.Load += new System.EventHandler(this.Category_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbCatE;
        private System.Windows.Forms.TextBox tbCatEName;
        private System.Windows.Forms.Label laCatEName;
        private System.Windows.Forms.Button bCatENeu;
        private System.Windows.Forms.Button bCatEDelete;
        private System.Windows.Forms.Button bCatESave;
    }
}