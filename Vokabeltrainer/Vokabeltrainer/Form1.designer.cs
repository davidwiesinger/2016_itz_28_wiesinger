﻿namespace Vokabeltrainer
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bt1_Check = new System.Windows.Forms.Button();
            this.bt2_Reload = new System.Windows.Forms.Button();
            this.menuStrip1_Main = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kategorienToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1_Main.SuspendLayout();
            this.SuspendLayout();
            // 
            // bt1_Check
            // 
            this.bt1_Check.Location = new System.Drawing.Point(136, 522);
            this.bt1_Check.Margin = new System.Windows.Forms.Padding(4);
            this.bt1_Check.Name = "bt1_Check";
            this.bt1_Check.Size = new System.Drawing.Size(141, 41);
            this.bt1_Check.TabIndex = 0;
            this.bt1_Check.Text = "Check";
            this.bt1_Check.UseVisualStyleBackColor = true;
            this.bt1_Check.Click += new System.EventHandler(this.bt1_Check_Click);
            // 
            // bt2_Reload
            // 
            this.bt2_Reload.Location = new System.Drawing.Point(335, 522);
            this.bt2_Reload.Margin = new System.Windows.Forms.Padding(4);
            this.bt2_Reload.Name = "bt2_Reload";
            this.bt2_Reload.Size = new System.Drawing.Size(128, 41);
            this.bt2_Reload.TabIndex = 2;
            this.bt2_Reload.Text = "Reload";
            this.bt2_Reload.UseVisualStyleBackColor = true;
            this.bt2_Reload.Click += new System.EventHandler(this.bt2_Reload_Click);
            // 
            // menuStrip1_Main
            // 
            this.menuStrip1_Main.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.optionenToolStripMenuItem});
            this.menuStrip1_Main.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1_Main.Name = "menuStrip1_Main";
            this.menuStrip1_Main.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1_Main.Size = new System.Drawing.Size(631, 28);
            this.menuStrip1_Main.TabIndex = 3;
            this.menuStrip1_Main.Text = "Menü";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem,
            this.kategorienToolStripMenuItem,
            this.beendenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // neuToolStripMenuItem
            // 
            this.neuToolStripMenuItem.Name = "neuToolStripMenuItem";
            this.neuToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.neuToolStripMenuItem.Text = "Neu...";
            this.neuToolStripMenuItem.Click += new System.EventHandler(this.neuToolStripMenuItem_Click);
            // 
            // kategorienToolStripMenuItem
            // 
            this.kategorienToolStripMenuItem.Name = "kategorienToolStripMenuItem";
            this.kategorienToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.kategorienToolStripMenuItem.Text = "Kategorien...";
            this.kategorienToolStripMenuItem.Click += new System.EventHandler(this.kategorienToolStripMenuItem_Click);
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            this.beendenToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.beendenToolStripMenuItem.Text = "Beenden";
            this.beendenToolStripMenuItem.Click += new System.EventHandler(this.beendenToolStripMenuItem_Click);
            // 
            // optionenToolStripMenuItem
            // 
            this.optionenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.einstellungenToolStripMenuItem});
            this.optionenToolStripMenuItem.Name = "optionenToolStripMenuItem";
            this.optionenToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.optionenToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.optionenToolStripMenuItem.Text = "Optionen";
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            this.einstellungenToolStripMenuItem.ShortcutKeyDisplayString = "Strg+E";
            this.einstellungenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.einstellungenToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.einstellungenToolStripMenuItem.Text = "Einstellungen";
            this.einstellungenToolStripMenuItem.Click += new System.EventHandler(this.einstellungenToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(631, 591);
            this.Controls.Add(this.bt2_Reload);
            this.Controls.Add(this.bt1_Check);
            this.Controls.Add(this.menuStrip1_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1_Main;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Vokabeltrainer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.F1_vktrainer_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.menuStrip1_Main.ResumeLayout(false);
            this.menuStrip1_Main.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt1_Check;
        private System.Windows.Forms.Button bt2_Reload;
        private System.Windows.Forms.MenuStrip menuStrip1_Main;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kategorienToolStripMenuItem;
    }
}

