﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.OleDb;

namespace Vokabeltrainer
{
    class Vokabeltrainer
    {       
        static private Vokabeltrainer voc = null;
        List<Vokabel> vokabeln;
        public delegate void NoteDel(string note);
        public event NoteDel Note;
        public delegate void Test(bool ifRight, Vokabel voc);
        public event Test VocabCheck;


        private Vokabeltrainer()
        { }

        static public Vokabeltrainer GetInstance()
        {
            if (voc == null)
            {
                voc = new Vokabeltrainer();
                return voc;
            }
            else
                return voc;
        }

        public void CheckVocab(List<Vokabel> vok, List<string> toCheck)
        {
            int right = 0;
            for (int i = 0; i < vok.Count; i++)
            {
                if (toCheck[i] == ((Options.GetInstance().TranslateMode == 0) ? vok[i].English : vok[i].German))
                {
                    right++;
                    VocabCheck(true, vok[i]);
                }
                else
                    VocabCheck(false, vok[i]);
            }
            Grading(((double)right / (double)vok.Count) * 100);

        }
        
        public void SortEntries()
        {
            Random rd = new Random();
            foreach (Vokabel v in Vokabeln)
            {
                v.Rand = rd.NextDouble();
            }
            Vokabeln.Sort(delegate (Vokabel a, Vokabel b) { return a.Rand.CompareTo(b.Rand); });
        }

        public void ReadFromDB(OleDbConnection con, int ccat)
        {
            string sql = "select ID, Deutsch, Englisch, Kategorie from Vokabel where Kategorie=" + ccat;
            OleDbCommand cmd = new OleDbCommand(sql, con);
            List<Vokabel> v = new List<Vokabel>();
            try
            {
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                { 
                    v.Add(new Vokabel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));
                }
                reader.Close();
            }
            catch (FileNotFoundException ex)
            {
                VTException vtex = new VTException("Fehler!", ex);
                throw vtex;
            }
            catch (Exception ex)
            {
                VTException vtex = new VTException("Fehler!", ex);
                throw vtex;
            }
            finally
            {
                if (v.Count > 0)
                {
                    vokabeln = v;
                }               
            }                                   
        }

        
        private void Grading(double percRight)
        {
            if (percRight > Options.GetInstance()["0"])
                Note(Options.GetInstance()[0]);
            else if (percRight > Options.GetInstance()["1"])
                Note(Options.GetInstance()[1]);
            else if (percRight > Options.GetInstance()["2"])
                Note(Options.GetInstance()[2]);
            else if (percRight > Options.GetInstance()["3"])
                Note(Options.GetInstance()[3]);
            else
                Note(Options.GetInstance()[4]);
        }

        public List<Vokabel> Vokabeln
        {
            get { return vokabeln; }
        }
    }
}
