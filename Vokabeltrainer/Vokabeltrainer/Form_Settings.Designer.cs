﻿namespace Vokabeltrainer
{ 
    partial class Form_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Settings));
            this.bt1_save = new System.Windows.Forms.Button();
            this.bt2_cancel = new System.Windows.Forms.Button();
            this.cBox1_Mode = new System.Windows.Forms.ComboBox();
            this.nUD1_vocCount = new System.Windows.Forms.NumericUpDown();
            this.label1_count = new System.Windows.Forms.Label();
            this.label2_lang = new System.Windows.Forms.Label();
            this.lb1_note1 = new System.Windows.Forms.Label();
            this.lb2_note2 = new System.Windows.Forms.Label();
            this.lb3_note3 = new System.Windows.Forms.Label();
            this.lb4_note4 = new System.Windows.Forms.Label();
            this.lb5_note5 = new System.Windows.Forms.Label();
            this.tB1_note1 = new System.Windows.Forms.TextBox();
            this.tB2_note2 = new System.Windows.Forms.TextBox();
            this.tB3_note3 = new System.Windows.Forms.TextBox();
            this.tB4_note4 = new System.Windows.Forms.TextBox();
            this.tB5_note5 = new System.Windows.Forms.TextBox();
            this.nUD2_note1Percent = new System.Windows.Forms.NumericUpDown();
            this.nUD3_note2Percent = new System.Windows.Forms.NumericUpDown();
            this.nUD4_note3Percent = new System.Windows.Forms.NumericUpDown();
            this.nUD5_note4Percent = new System.Windows.Forms.NumericUpDown();
            this.lb6_text = new System.Windows.Forms.Label();
            this.lb7_percent = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nUD1_vocCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD2_note1Percent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD3_note2Percent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD4_note3Percent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD5_note4Percent)).BeginInit();
            this.SuspendLayout();
            // 
            // bt1_save
            // 
            this.bt1_save.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bt1_save.Location = new System.Drawing.Point(58, 307);
            this.bt1_save.Name = "bt1_save";
            this.bt1_save.Size = new System.Drawing.Size(75, 23);
            this.bt1_save.TabIndex = 0;
            this.bt1_save.Text = "Speichern";
            this.bt1_save.UseVisualStyleBackColor = true;
            this.bt1_save.Click += new System.EventHandler(this.bt1_save_Click);
            // 
            // bt2_cancel
            // 
            this.bt2_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt2_cancel.Location = new System.Drawing.Point(174, 307);
            this.bt2_cancel.Name = "bt2_cancel";
            this.bt2_cancel.Size = new System.Drawing.Size(75, 23);
            this.bt2_cancel.TabIndex = 1;
            this.bt2_cancel.Text = "Abbrechen";
            this.bt2_cancel.UseVisualStyleBackColor = true;
            // 
            // cBox1_Mode
            // 
            this.cBox1_Mode.FormattingEnabled = true;
            this.cBox1_Mode.Location = new System.Drawing.Point(129, 91);
            this.cBox1_Mode.Name = "cBox1_Mode";
            this.cBox1_Mode.Size = new System.Drawing.Size(120, 21);
            this.cBox1_Mode.TabIndex = 2;
            this.cBox1_Mode.SelectedIndexChanged += new System.EventHandler(this.cBox1_Mode_SelectedIndexChanged);
            // 
            // nUD1_vocCount
            // 
            this.nUD1_vocCount.Location = new System.Drawing.Point(129, 58);
            this.nUD1_vocCount.Name = "nUD1_vocCount";
            this.nUD1_vocCount.Size = new System.Drawing.Size(120, 20);
            this.nUD1_vocCount.TabIndex = 3;
            this.nUD1_vocCount.ValueChanged += new System.EventHandler(this.nUD1_vocCount_ValueChanged);
            // 
            // label1_count
            // 
            this.label1_count.AutoSize = true;
            this.label1_count.Location = new System.Drawing.Point(13, 58);
            this.label1_count.Name = "label1_count";
            this.label1_count.Size = new System.Drawing.Size(77, 13);
            this.label1_count.TabIndex = 4;
            this.label1_count.Text = "Vokabelanzahl";
            // 
            // label2_lang
            // 
            this.label2_lang.AutoSize = true;
            this.label2_lang.Location = new System.Drawing.Point(13, 91);
            this.label2_lang.Name = "label2_lang";
            this.label2_lang.Size = new System.Drawing.Size(80, 13);
            this.label2_lang.TabIndex = 5;
            this.label2_lang.Text = "Sprachauswahl";
            // 
            // lb1_note1
            // 
            this.lb1_note1.AutoSize = true;
            this.lb1_note1.Location = new System.Drawing.Point(13, 152);
            this.lb1_note1.Name = "lb1_note1";
            this.lb1_note1.Size = new System.Drawing.Size(42, 13);
            this.lb1_note1.TabIndex = 6;
            this.lb1_note1.Text = "Note 1:";
            // 
            // lb2_note2
            // 
            this.lb2_note2.AutoSize = true;
            this.lb2_note2.Location = new System.Drawing.Point(13, 178);
            this.lb2_note2.Name = "lb2_note2";
            this.lb2_note2.Size = new System.Drawing.Size(42, 13);
            this.lb2_note2.TabIndex = 7;
            this.lb2_note2.Text = "Note 2:";
            // 
            // lb3_note3
            // 
            this.lb3_note3.AutoSize = true;
            this.lb3_note3.Location = new System.Drawing.Point(13, 204);
            this.lb3_note3.Name = "lb3_note3";
            this.lb3_note3.Size = new System.Drawing.Size(42, 13);
            this.lb3_note3.TabIndex = 8;
            this.lb3_note3.Text = "Note 3:";
            // 
            // lb4_note4
            // 
            this.lb4_note4.AutoSize = true;
            this.lb4_note4.Location = new System.Drawing.Point(13, 230);
            this.lb4_note4.Name = "lb4_note4";
            this.lb4_note4.Size = new System.Drawing.Size(42, 13);
            this.lb4_note4.TabIndex = 9;
            this.lb4_note4.Text = "Note 4:";
            // 
            // lb5_note5
            // 
            this.lb5_note5.AutoSize = true;
            this.lb5_note5.Location = new System.Drawing.Point(13, 256);
            this.lb5_note5.Name = "lb5_note5";
            this.lb5_note5.Size = new System.Drawing.Size(42, 13);
            this.lb5_note5.TabIndex = 10;
            this.lb5_note5.Text = "Note 5:";
            // 
            // tB1_note1
            // 
            this.tB1_note1.Location = new System.Drawing.Point(58, 152);
            this.tB1_note1.Name = "tB1_note1";
            this.tB1_note1.Size = new System.Drawing.Size(191, 20);
            this.tB1_note1.TabIndex = 11;
            this.tB1_note1.TextChanged += new System.EventHandler(this.tB1_note1_TextChanged);
            // 
            // tB2_note2
            // 
            this.tB2_note2.Location = new System.Drawing.Point(58, 178);
            this.tB2_note2.Name = "tB2_note2";
            this.tB2_note2.Size = new System.Drawing.Size(191, 20);
            this.tB2_note2.TabIndex = 12;
            this.tB2_note2.TextChanged += new System.EventHandler(this.tB2_note2_TextChanged);
            // 
            // tB3_note3
            // 
            this.tB3_note3.Location = new System.Drawing.Point(58, 204);
            this.tB3_note3.Name = "tB3_note3";
            this.tB3_note3.Size = new System.Drawing.Size(191, 20);
            this.tB3_note3.TabIndex = 13;
            this.tB3_note3.TextChanged += new System.EventHandler(this.tB3_note3_TextChanged);
            // 
            // tB4_note4
            // 
            this.tB4_note4.Location = new System.Drawing.Point(58, 230);
            this.tB4_note4.Name = "tB4_note4";
            this.tB4_note4.Size = new System.Drawing.Size(191, 20);
            this.tB4_note4.TabIndex = 14;
            this.tB4_note4.TextChanged += new System.EventHandler(this.tB4_note4_TextChanged);
            // 
            // tB5_note5
            // 
            this.tB5_note5.Location = new System.Drawing.Point(58, 256);
            this.tB5_note5.Name = "tB5_note5";
            this.tB5_note5.Size = new System.Drawing.Size(191, 20);
            this.tB5_note5.TabIndex = 15;
            this.tB5_note5.TextChanged += new System.EventHandler(this.tB5_note5_TextChanged);
            // 
            // nUD2_note1Percent
            // 
            this.nUD2_note1Percent.Location = new System.Drawing.Point(273, 152);
            this.nUD2_note1Percent.Name = "nUD2_note1Percent";
            this.nUD2_note1Percent.Size = new System.Drawing.Size(50, 20);
            this.nUD2_note1Percent.TabIndex = 16;
            this.nUD2_note1Percent.ValueChanged += new System.EventHandler(this.nUD2_note1Percent_ValueChanged);
            // 
            // nUD3_note2Percent
            // 
            this.nUD3_note2Percent.Location = new System.Drawing.Point(273, 178);
            this.nUD3_note2Percent.Name = "nUD3_note2Percent";
            this.nUD3_note2Percent.Size = new System.Drawing.Size(50, 20);
            this.nUD3_note2Percent.TabIndex = 17;
            this.nUD3_note2Percent.ValueChanged += new System.EventHandler(this.nUD3_note2Percent_ValueChanged);
            // 
            // nUD4_note3Percent
            // 
            this.nUD4_note3Percent.Location = new System.Drawing.Point(273, 204);
            this.nUD4_note3Percent.Name = "nUD4_note3Percent";
            this.nUD4_note3Percent.Size = new System.Drawing.Size(50, 20);
            this.nUD4_note3Percent.TabIndex = 18;
            this.nUD4_note3Percent.ValueChanged += new System.EventHandler(this.nUD4_note3Percent_ValueChanged);
            // 
            // nUD5_note4Percent
            // 
            this.nUD5_note4Percent.Location = new System.Drawing.Point(273, 230);
            this.nUD5_note4Percent.Name = "nUD5_note4Percent";
            this.nUD5_note4Percent.Size = new System.Drawing.Size(50, 20);
            this.nUD5_note4Percent.TabIndex = 19;
            this.nUD5_note4Percent.ValueChanged += new System.EventHandler(this.nUD5_note4Percent_ValueChanged);
            // 
            // lb6_text
            // 
            this.lb6_text.AutoSize = true;
            this.lb6_text.Location = new System.Drawing.Point(58, 133);
            this.lb6_text.Name = "lb6_text";
            this.lb6_text.Size = new System.Drawing.Size(31, 13);
            this.lb6_text.TabIndex = 21;
            this.lb6_text.Text = "Text:";
            // 
            // lb7_percent
            // 
            this.lb7_percent.AutoSize = true;
            this.lb7_percent.Location = new System.Drawing.Point(273, 132);
            this.lb7_percent.Name = "lb7_percent";
            this.lb7_percent.Size = new System.Drawing.Size(46, 13);
            this.lb7_percent.TabIndex = 22;
            this.lb7_percent.Text = "Prozent:";
            // 
            // Form_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 347);
            this.Controls.Add(this.lb7_percent);
            this.Controls.Add(this.lb6_text);
            this.Controls.Add(this.nUD5_note4Percent);
            this.Controls.Add(this.nUD4_note3Percent);
            this.Controls.Add(this.nUD3_note2Percent);
            this.Controls.Add(this.nUD2_note1Percent);
            this.Controls.Add(this.tB5_note5);
            this.Controls.Add(this.tB4_note4);
            this.Controls.Add(this.tB3_note3);
            this.Controls.Add(this.tB2_note2);
            this.Controls.Add(this.tB1_note1);
            this.Controls.Add(this.lb5_note5);
            this.Controls.Add(this.lb4_note4);
            this.Controls.Add(this.lb3_note3);
            this.Controls.Add(this.lb2_note2);
            this.Controls.Add(this.lb1_note1);
            this.Controls.Add(this.label2_lang);
            this.Controls.Add(this.label1_count);
            this.Controls.Add(this.nUD1_vocCount);
            this.Controls.Add(this.cBox1_Mode);
            this.Controls.Add(this.bt2_cancel);
            this.Controls.Add(this.bt1_save);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_Settings";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Einstellungen";
            ((System.ComponentModel.ISupportInitialize)(this.nUD1_vocCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD2_note1Percent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD3_note2Percent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD4_note3Percent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD5_note4Percent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt1_save;
        private System.Windows.Forms.Button bt2_cancel;
        private System.Windows.Forms.ComboBox cBox1_Mode;
        private System.Windows.Forms.NumericUpDown nUD1_vocCount;
        private System.Windows.Forms.Label label1_count;
        private System.Windows.Forms.Label label2_lang;
        private System.Windows.Forms.Label lb1_note1;
        private System.Windows.Forms.Label lb2_note2;
        private System.Windows.Forms.Label lb3_note3;
        private System.Windows.Forms.Label lb4_note4;
        private System.Windows.Forms.Label lb5_note5;
        private System.Windows.Forms.TextBox tB1_note1;
        private System.Windows.Forms.TextBox tB2_note2;
        private System.Windows.Forms.TextBox tB3_note3;
        private System.Windows.Forms.TextBox tB4_note4;
        private System.Windows.Forms.TextBox tB5_note5;
        private System.Windows.Forms.NumericUpDown nUD2_note1Percent;
        private System.Windows.Forms.NumericUpDown nUD3_note2Percent;
        private System.Windows.Forms.NumericUpDown nUD4_note3Percent;
        private System.Windows.Forms.NumericUpDown nUD5_note4Percent;
        private System.Windows.Forms.Label lb6_text;
        private System.Windows.Forms.Label lb7_percent;
    }
}