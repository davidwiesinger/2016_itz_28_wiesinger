﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vokabeltrainer
{
    public class Vokabel
    {
        private int id, kat;
        private string english;
        private string german;
        private double rand;

        public Vokabel(int id, string german, string english, int kat)
        {
            rand = 0;
            English = english;
            German = german;
            ID = id;
            Kat = kat;
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string English
        {
            get { return english; }
            set { english = value; }
        }
        public string German
        {
            get { return german; }
            set { german = value; }
        }
        public int Kat
        {
            get { return kat; }
            set { kat = value; }
        }
        public double Rand
        {
            get { return rand; }
            set { rand = value; }
        }

    }
}
