﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Vokabeltrainer
{
    class Options
    {
        private static Options opt = null;
        private int vocabCount;
        private bool isDirty = false;
        private int translateMode;
        private double[] notenperc = new double[4];      
        private string[] noten = new string[5];


        private Options()
        {
            ReadOptions();
        }

        static public Options GetInstance()
        {
           
            if (opt == null)
            {
                opt = new Options();
            }
            return opt;
        }

        public void ReadOptions()
        {

            int i = 0;
            string z;
            List<string> r = new List<string>();
            try
            {
                StreamReader sr = new StreamReader("options.txt");
                while (sr.Peek() != -1)
                {
                    z = sr.ReadLine();
                    if (z != "")
                    {
                        r.Add(z.Substring(z.IndexOf('=') + 2));
                        i++;
                    }
                }
                sr.Close();
            }
            catch (FileNotFoundException ex)
            {
                VTException vtex = new VTException("Fehler!", ex);                
            }           
            catch (Exception ex)
            {
                VTException vtex = new VTException("Fehler!", ex);               
            }
            finally
            {
                if (r.Count > 11)
                {
                    VocabCount = ((r.Count > 0) ? Convert.ToInt32(r[0]) : 5);
                    TranslateMode = ((1 < r.Count) ? Convert.ToInt32(r[1]) : 0);
                    this[0] = ((r[2] == null) ? "Sehr gut" : r[2]);
                    this["0"] =((r[3] == null) ? 91 : Convert.ToDouble(r[3]));
                    this[1] = ((r[4] == null) ? "Gut" : r[4]);
                    this["1"] = ((r[5] == null) ? 73 : Convert.ToDouble(r[5]));
                    this[2] =  ((r[6] == null) ? "Befriedigend" : r[6]);
                    this["2"] = ((r[7] == null) ? 65 : Convert.ToDouble(r[7])) ;
                    this[3] = ((r[8] == null) ? "Genügend" : r[8]);
                    this["3"] = ((r[9] == null) ? 50 : Convert.ToDouble(r[9]));
                    this[4] = ((r[10] == null) ? "Nicht genügend" : r[10]);
                }
                else
                {
                    VocabCount = 5;
                    TranslateMode = 0;
                    this[0] = "Sehr gut";
                    this["0"] = 91;
                    this[1] = "Gut";
                    this["1"] = 73;
                    this[2] = "Befriedigend";
                    this["2"] = 65;
                    this[3] = "Genügend";
                    this["3"] = 50;
                    this[4] = "Nicht genügend";
                }
                isDirty = false;
            }
        }

        public void WriteOptions()
        {
            if (isDirty)
            {
                try
                {
                    StreamWriter sw = new StreamWriter("options.txt");
                    sw.WriteLine("VocabCount = " + VocabCount);
                    sw.WriteLine("Translate Mode = " + TranslateMode);
                    sw.WriteLine("Note 1 = " + this[0]);
                    sw.WriteLine("Note 1 Percent = " + this["0"]);
                    sw.WriteLine("Note 2 = " + this[1]);
                    sw.WriteLine("Note 2 Percent = " + this["1"]);
                    sw.WriteLine("Note 3 = " + this[2]);
                    sw.WriteLine("Note 3 Percent = " + this["2"]);
                    sw.WriteLine("Note 4 = " + this[3]);
                    sw.WriteLine("Note 4 Percent = " + this["3"]);
                    sw.WriteLine("Note 5 = " + this[4]);

                    sw.Flush();
                    sw.Close();
                }
                catch (Exception ex)
                {
                    VTException vtex = new VTException("Fehler!", ex);

                }
                finally
                {
                    isDirty = false;
                }
            }
        }

        public int VocabCount
        {
            get { return vocabCount;  }
            set {
                if (value != vocabCount)
                {
                    isDirty = true;
                    vocabCount = value;
                }                
            }
        }

        public int TranslateMode
        {
            get { return translateMode; }
            set
            {
                if (value != translateMode)
                {
                    isDirty = true;
                    translateMode = value;
                }
            }
        }

        public string this[int notenIndex]
        {
            get { return noten[notenIndex]; }
            set
            {
                if (noten[notenIndex] != value)
                {
                    isDirty = true;
                    noten[notenIndex] = value;
                }
            }
        }

        public double this[string notenpercIndex]
        {
            get { return notenperc[Convert.ToInt32(notenpercIndex)]; }
            set
            {
                if (value != notenperc[Convert.ToInt32(notenpercIndex)])
                {
                    isDirty = true;
                    notenperc[Convert.ToInt32(notenpercIndex)] = value;
                }
            }
        }
    }
}
