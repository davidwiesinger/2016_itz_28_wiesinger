﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Vokabeltrainer
{
    public partial class Category_Form : Form
    {
        Category act_cat = null;
        public Category_Form()
        {
            InitializeComponent();
        }

        private void Category_Form_Load(object sender, EventArgs e)
        {
            DatabaseConnection.GetInstance().Open();
            FilllbCatE();
        }

        private void FilllbCatE()
        {
            lbCatE.Items.Clear();
            string sql = "select ID, Beschreibung from Kategorie";
            OleDbCommand cmd = new OleDbCommand(sql, DatabaseConnection.GetInstance().Con);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                lbCatE.Items.Add(new Category(reader.GetInt32(0), reader.GetString(1)));
            }
            reader.Close();
        }

        private void lbCatE_SelectedIndexChanged(object sender, EventArgs e)
        {
            act_cat = (Category)lbCatE.SelectedItem;
            tbCatEName.Enabled = true;
            tbCatEName.Text = act_cat.Desc;
            bCatEDelete.Enabled = true;
        }

        private void bCatENeu_Click(object sender, EventArgs e)
        {
            act_cat = new Category(0, "");
            tbCatEName.Enabled = true;
            tbCatEName.Text = act_cat.Desc;
        }

        private void bCatESave_Click(object sender, EventArgs e)
        {
            if (act_cat.ID == 0)
            {
                string sql = "insert into Kategorie (Beschreibung) values ('" + tbCatEName.Text + "')";
                OleDbCommand cmd = new OleDbCommand(sql, DatabaseConnection.GetInstance().Con);
                cmd.ExecuteNonQuery();
            }
            if (act_cat.ID > 0)
            {
                string sql = "update Kategorie set Beschreibung='" + tbCatEName.Text + "' where ID=" + act_cat.ID;
                OleDbCommand cmd = new OleDbCommand(sql, DatabaseConnection.GetInstance().Con);
                cmd.ExecuteNonQuery();
            }
            tbCatEName.Clear();
            tbCatEName.Enabled = false;
            FilllbCatE();
        }

        private void bCatEDelete_Click(object sender, EventArgs e)
        {
            string sql = "delete from Kategorie where ID=" + act_cat.ID;
            OleDbCommand cmd = new OleDbCommand(sql, DatabaseConnection.GetInstance().Con);
            cmd.ExecuteNonQuery();
            tbCatEName.Clear();
            tbCatEName.Enabled = false;
            FilllbCatE();
        }
    }
}
