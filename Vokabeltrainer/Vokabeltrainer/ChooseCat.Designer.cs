﻿namespace Vokabeltrainer
{
    partial class ChooseCat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCCat = new System.Windows.Forms.ComboBox();
            this.b1_CCat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbCCat
            // 
            this.cbCCat.FormattingEnabled = true;
            this.cbCCat.Location = new System.Drawing.Point(12, 12);
            this.cbCCat.Name = "cbCCat";
            this.cbCCat.Size = new System.Drawing.Size(350, 24);
            this.cbCCat.TabIndex = 1;
            this.cbCCat.SelectedIndexChanged += new System.EventHandler(this.cbCCat_SelectedIndexChanged);
            // 
            // b1_CCat
            // 
            this.b1_CCat.Location = new System.Drawing.Point(108, 42);
            this.b1_CCat.Name = "b1_CCat";
            this.b1_CCat.Size = new System.Drawing.Size(176, 23);
            this.b1_CCat.TabIndex = 1;
            this.b1_CCat.Text = "Kategorie auswählen";
            this.b1_CCat.UseVisualStyleBackColor = true;
            this.b1_CCat.Click += new System.EventHandler(this.b1_CCat_Click);
            // 
            // ChooseCat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 72);
            this.Controls.Add(this.b1_CCat);
            this.Controls.Add(this.cbCCat);
            this.Name = "ChooseCat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Kategorie auswählen";
            this.Load += new System.EventHandler(this.ChooseCat_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCCat;
        private System.Windows.Forms.Button b1_CCat;
    }
}