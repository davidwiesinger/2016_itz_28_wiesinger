﻿namespace HTLBibliothek
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtbISBNupd = new System.Windows.Forms.MaskedTextBox();
            this.bUpdate = new System.Windows.Forms.Button();
            this.tbPreisupd = new System.Windows.Forms.TextBox();
            this.laPreisupd = new System.Windows.Forms.Label();
            this.tbJahrupd = new System.Windows.Forms.TextBox();
            this.tbAutorupd = new System.Windows.Forms.TextBox();
            this.laJahrupd = new System.Windows.Forms.Label();
            this.laAutorupd = new System.Windows.Forms.Label();
            this.tbTitelupd = new System.Windows.Forms.TextBox();
            this.laTitelupd = new System.Windows.Forms.Label();
            this.laISBNupd = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mtbISBNupd
            // 
            this.mtbISBNupd.AsciiOnly = true;
            this.mtbISBNupd.Location = new System.Drawing.Point(12, 28);
            this.mtbISBNupd.Mask = "0-000000-00-0";
            this.mtbISBNupd.Name = "mtbISBNupd";
            this.mtbISBNupd.Size = new System.Drawing.Size(264, 22);
            this.mtbISBNupd.TabIndex = 18;
            // 
            // bUpdate
            // 
            this.bUpdate.Location = new System.Drawing.Point(12, 274);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(264, 29);
            this.bUpdate.TabIndex = 26;
            this.bUpdate.Text = "Ändern";
            this.bUpdate.UseVisualStyleBackColor = true;
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // tbPreisupd
            // 
            this.tbPreisupd.Location = new System.Drawing.Point(12, 237);
            this.tbPreisupd.Name = "tbPreisupd";
            this.tbPreisupd.Size = new System.Drawing.Size(264, 22);
            this.tbPreisupd.TabIndex = 24;
            // 
            // laPreisupd
            // 
            this.laPreisupd.AutoSize = true;
            this.laPreisupd.Location = new System.Drawing.Point(9, 217);
            this.laPreisupd.Name = "laPreisupd";
            this.laPreisupd.Size = new System.Drawing.Size(40, 17);
            this.laPreisupd.TabIndex = 27;
            this.laPreisupd.Text = "Preis";
            // 
            // tbJahrupd
            // 
            this.tbJahrupd.Location = new System.Drawing.Point(12, 185);
            this.tbJahrupd.Name = "tbJahrupd";
            this.tbJahrupd.Size = new System.Drawing.Size(264, 22);
            this.tbJahrupd.TabIndex = 22;
            // 
            // tbAutorupd
            // 
            this.tbAutorupd.Location = new System.Drawing.Point(12, 135);
            this.tbAutorupd.Name = "tbAutorupd";
            this.tbAutorupd.Size = new System.Drawing.Size(264, 22);
            this.tbAutorupd.TabIndex = 21;
            // 
            // laJahrupd
            // 
            this.laJahrupd.AutoSize = true;
            this.laJahrupd.Location = new System.Drawing.Point(9, 165);
            this.laJahrupd.Name = "laJahrupd";
            this.laJahrupd.Size = new System.Drawing.Size(118, 17);
            this.laJahrupd.TabIndex = 25;
            this.laJahrupd.Text = "Erscheinungsjahr";
            // 
            // laAutorupd
            // 
            this.laAutorupd.AutoSize = true;
            this.laAutorupd.Location = new System.Drawing.Point(9, 115);
            this.laAutorupd.Name = "laAutorupd";
            this.laAutorupd.Size = new System.Drawing.Size(42, 17);
            this.laAutorupd.TabIndex = 23;
            this.laAutorupd.Text = "Autor";
            // 
            // tbTitelupd
            // 
            this.tbTitelupd.Location = new System.Drawing.Point(12, 81);
            this.tbTitelupd.Name = "tbTitelupd";
            this.tbTitelupd.Size = new System.Drawing.Size(264, 22);
            this.tbTitelupd.TabIndex = 19;
            // 
            // laTitelupd
            // 
            this.laTitelupd.AutoSize = true;
            this.laTitelupd.Location = new System.Drawing.Point(9, 61);
            this.laTitelupd.Name = "laTitelupd";
            this.laTitelupd.Size = new System.Drawing.Size(35, 17);
            this.laTitelupd.TabIndex = 20;
            this.laTitelupd.Text = "Titel";
            // 
            // laISBNupd
            // 
            this.laISBNupd.AutoSize = true;
            this.laISBNupd.Location = new System.Drawing.Point(9, 8);
            this.laISBNupd.Name = "laISBNupd";
            this.laISBNupd.Size = new System.Drawing.Size(39, 17);
            this.laISBNupd.TabIndex = 17;
            this.laISBNupd.Text = "ISBN";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 316);
            this.Controls.Add(this.mtbISBNupd);
            this.Controls.Add(this.bUpdate);
            this.Controls.Add(this.tbPreisupd);
            this.Controls.Add(this.laPreisupd);
            this.Controls.Add(this.tbJahrupd);
            this.Controls.Add(this.tbAutorupd);
            this.Controls.Add(this.laJahrupd);
            this.Controls.Add(this.laAutorupd);
            this.Controls.Add(this.tbTitelupd);
            this.Controls.Add(this.laTitelupd);
            this.Controls.Add(this.laISBNupd);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox mtbISBNupd;
        private System.Windows.Forms.Button bUpdate;
        private System.Windows.Forms.TextBox tbPreisupd;
        private System.Windows.Forms.Label laPreisupd;
        private System.Windows.Forms.TextBox tbJahrupd;
        private System.Windows.Forms.TextBox tbAutorupd;
        private System.Windows.Forms.Label laJahrupd;
        private System.Windows.Forms.Label laAutorupd;
        private System.Windows.Forms.TextBox tbTitelupd;
        private System.Windows.Forms.Label laTitelupd;
        private System.Windows.Forms.Label laISBNupd;
    }
}