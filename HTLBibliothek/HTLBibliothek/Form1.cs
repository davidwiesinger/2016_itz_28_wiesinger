﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HTLBibliothek
{
    public partial class Form1 : Form
    {
        OleDbConnection conn = null;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            conn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb");
            conn.Open();
            cbThemenID_fill(conn);
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            conn.Close();
        }
        private void cbThemenID_fill(OleDbConnection conn)
        {
            string sql = "select ID, Bezeichnung from Themengruppe";
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int tid = reader.GetInt32(0);
                string tbez = reader.GetString(1);
                cbThemenID.Items.Add(new ThemenID(tid, tbez));
            }
            reader.Close();
        }
        private void cbThemenID_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbTIDBuecher.Items.Clear();
            string sql = "select BuchNr, ThemenID, ISBN, Titel, Autor, Erscheinungsjahr, Preis from Buch where ThemenID=" + ((ThemenID)cbThemenID.SelectedItem).ID;
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int bnr = reader.GetInt32(0);
                int tid = reader.GetInt32(1);
                string isbn = reader.GetString(2);
                string titel = reader.GetString(3);
                string autor = reader.GetString(4);
                int jahr = reader.GetInt32(5);
                float preis = reader.GetFloat(6);
                lbTIDBuecher.Items.Add(new Buch(bnr, tid, isbn, titel, autor, jahr, preis));
            }
            reader.Close();
        }
        private void lbTIDBuecher_SelectedIndexChanged(object sender, EventArgs e)
        {
            Buch bf2 = (Buch)lbTIDBuecher.SelectedItem;
            Form2 f2 = new Form2(bf2);
            f2.ShowDialog();
        }
    }
}