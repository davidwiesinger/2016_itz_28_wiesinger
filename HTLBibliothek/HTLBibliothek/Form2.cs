﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HTLBibliothek
{
    public partial class Form2 : Form
    {
        OleDbConnection conn = null;
        public Buch Buchupd { get; set; }
        public Form2()
        {
            InitializeComponent();
            this.Text = "Verwaltung";
        }
        public Form2(Buch b)
        {
            InitializeComponent();
            Buchupd = b;
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            conn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb");
            conn.Open();
            mtbISBNupd.Text = Buchupd.ISBN;
            tbTitelupd.Text = Buchupd.Titel;
            tbAutorupd.Text = Buchupd.Autor;
            tbJahrupd.Text = Convert.ToString(Buchupd.Jahr);
            tbPreisupd.Text = Buchupd.Preis.ToString("n2");
        }
        private void bUpdate_Click(object sender, EventArgs e)
        {
            int BuchNr = Buchupd.BuchNr;
            string ISBNupd = mtbISBNupd.Text;
            string Titelupd = tbTitelupd.Text;
            string Autorupd = tbAutorupd.Text;
            int Jahrupd = Convert.ToInt32(tbJahrupd.Text);
            float Preisupd = Convert.ToSingle(tbPreisupd.Text);
            string sql = "update Buch set ISBN='" + ISBNupd + "', Titel='" + Titelupd + "', Autor='" + Autorupd + "', Erscheinungsjahr=" + Jahrupd + ", Preis='" + Preisupd + "' where BuchNr = " + BuchNr;
            //update Buch set ISBN='3-902116-44-0', Titel='Einführung in die Kostenrechnung', Autor='Simandl Gustav', Erscheinungsjahr=2005, Preis='19,9' where BuchNr=1
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            cmd.ExecuteNonQuery();
            Dispose();
        }
        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            conn.Close();
        }
    }
}
