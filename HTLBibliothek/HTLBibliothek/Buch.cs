﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTLBibliothek
{   //Klasse um Bücher zu speichern
    public class Buch
    {
        public int BuchNr { get; set; }
        public int TID { get; set; }
        public string ISBN { get; set; }
        public string Titel { get; set; }
        public string Autor { get; set; }
        public int Jahr { get; set; }
        public float Preis { get; set; }

        public Buch(int bnr, int tid, string isbn, string titel, string autor, int jahr, float preis)
        {
            BuchNr = bnr;
            TID = tid;
            ISBN = isbn;
            Titel = titel;
            Autor = autor;
            Jahr = jahr;
            Preis = preis;
        }

        public override string ToString()
        {
            return Titel;
        }
    }
}
