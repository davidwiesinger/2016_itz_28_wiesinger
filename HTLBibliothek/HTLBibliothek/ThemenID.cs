﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTLBibliothek
{   //Klasse um ThemenIDs zu speichern
    class ThemenID
    {
        public int ID { get; set; }
        public string Bez { get; set; }
        public ThemenID(int id, string bez)
        {
            ID = id;
            Bez = bez;
        }
        public override string ToString()
        {
            return ID + " - " + Bez;
        }
    }
}
