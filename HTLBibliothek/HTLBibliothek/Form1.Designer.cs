﻿namespace HTLBibliothek
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbThemenID = new System.Windows.Forms.ComboBox();
            this.lbTIDBuecher = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // cbThemenID
            // 
            this.cbThemenID.FormattingEnabled = true;
            this.cbThemenID.Location = new System.Drawing.Point(13, 13);
            this.cbThemenID.Name = "cbThemenID";
            this.cbThemenID.Size = new System.Drawing.Size(299, 24);
            this.cbThemenID.TabIndex = 0;
            this.cbThemenID.SelectedIndexChanged += new System.EventHandler(this.cbThemenID_SelectedIndexChanged);
            // 
            // lbTIDBuecher
            // 
            this.lbTIDBuecher.FormattingEnabled = true;
            this.lbTIDBuecher.ItemHeight = 16;
            this.lbTIDBuecher.Location = new System.Drawing.Point(13, 44);
            this.lbTIDBuecher.Name = "lbTIDBuecher";
            this.lbTIDBuecher.Size = new System.Drawing.Size(299, 228);
            this.lbTIDBuecher.TabIndex = 1;
            this.lbTIDBuecher.SelectedIndexChanged += new System.EventHandler(this.lbTIDBuecher_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 282);
            this.Controls.Add(this.lbTIDBuecher);
            this.Controls.Add(this.cbThemenID);
            this.Name = "Form1";
            this.Text = "Verwaltung";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbThemenID;
        private System.Windows.Forms.ListBox lbTIDBuecher;
    }
}

