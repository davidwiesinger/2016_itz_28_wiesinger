﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace ArtikelDB
{
    class Article
    {
        bool isDirty = false, isNew = false;
        int id, count;
        string ean, desc;
        DateTime inStore;
        double salePrice;
        //Category category;
        int category;

        public Article()
        {

        }

        public Article(int id, string ean, string desc, int count, DateTime inStore, double salePrice, int category)
        {
            ID = id;
            EAN = ean;
            Description = desc;
            Count = count;
            InStore = inStore;
            SalePrice = salePrice;
            isDirty = false;
        }
        public Article(int id, string desc)
        {
            ID = id;
            Description = desc;
            isDirty = false;
        }

        public int WriteToDB(OleDbConnection con)
        {
            int res = 0;
            if (isNew)
            {
                res = 10;
                string sql = "INSERT INTO Artikel (EAN, Bezeichnung, Lager, ImSortiment, VKP, Kategorie) VALUES ('"+ EAN +"', '"+Description +"',"+ Count +", '"+InStore.ToShortDateString() +"', '"+ SalePrice+"',"+ Category + ")";
                try
                {
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    res += cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                    DBException dbex = new DBException("Error!", ex);
                    throw dbex;
                }
            }
            else if (isDirty)
            {
                res = 20;
                string sql = "UPDATE Artikel SET EAN = '"+EAN+"', Bezeichnung ='"+Description+"', Lager = "+Count+", ImSortiment='"+InStore.ToShortDateString()+"', VKP= '"+SalePrice+"' , Kategorie = '"+Category+"' WHERE ID = " + ID;
                try
                {
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    res += cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                    DBException dbex = new DBException("Error!", ex);
                    throw dbex;
                }
            }
            return res;
        }

        public int DeleteFromDB(OleDbConnection con)
        {
            string sql = "DELETE FROM Artikel WHERE ID = " + ID;
            try
            {
                OleDbCommand cmd = new OleDbCommand(sql, con);
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                DBException dbex = new DBException("Error!", ex);
                throw dbex;
            }
        }

        public override string ToString()
        {
            if (Description.Length > 27)
            {
                return Description.Substring(0, 27) + "...";
            }
            else
            {
                return Description;
            }

        }

        #region Props
        public bool IsNew
        { 
            set { isNew = value; }
        }

        public int ID
        {
            get { return id; }
            set {
                if (value != id)
                {
                    isDirty = true;
                    id = value;
                }
            }
        }

        public int Count
        {
            get { return count; }
            set {
                if (value != count)
                {
                    isDirty = true;
                    count = value;
                }
            }
        }

        public string EAN
        {
            get { return ean; }
            set {
                if (value != ean)
                {
                    isDirty = true;
                    ean = value;
                }
            }
        }

        public string Description
        {
            get { return desc; }
            set {
                if (value != desc)
                {
                    isDirty = true;
                    desc = value;
                }
            }
        }

        public DateTime InStore
        {
            get { return inStore; }
            set {
                if (value != inStore)
                {
                    isDirty = true;
                    inStore = value;
                }
            }
        }

        public double SalePrice
        {
            get { return salePrice; }
            set {
                if (value != salePrice)
                {
                    isDirty = true;
                    salePrice = value;
                }
            }
        }


        public int Category
        {
            get { return category; }
            set {
                if (value != category)
                {
                    isDirty = true;
                    category = value;
                }  
            }
        }
        #endregion

    }
}
