﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace ArtikelDB
{
    class DatabaseConnection
    {
        private static DatabaseConnection dbcon = null;
        OleDbConnection con = null;
        

        private DatabaseConnection()
        {
            con = new OleDbConnection(@"Provider=Microsoft.JET.OLEDB.4.0;data source=artikelDB.mdb");         
        }

        public static DatabaseConnection GetInstance()
        {
            if (dbcon == null)
            {
                    dbcon = new DatabaseConnection();                
            }
            return dbcon;
        }

        public void Open()
        {
            if (con.State != System.Data.ConnectionState.Open)
            {
                try
                {
                    con.Open();
                }
                catch (Exception ex)
                {
                    DBException dbex = new DBException("Error!", ex);
                    throw dbex;
                }
            }
        }

        public void Close()
        {
            Con.Close();
        }


        public OleDbConnection Con
        {
            get { return con; }
        }
    }
}
