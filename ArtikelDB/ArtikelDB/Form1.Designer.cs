﻿namespace ArtikelDB
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb1_artVerv = new System.Windows.Forms.Label();
            this.liBo1_artList = new System.Windows.Forms.ListBox();
            this.lb2_ean = new System.Windows.Forms.Label();
            this.lb3_bezeichnung = new System.Windows.Forms.Label();
            this.lb4_vkp = new System.Windows.Forms.Label();
            this.lb5_sortiment = new System.Windows.Forms.Label();
            this.lb6_lst = new System.Windows.Forms.Label();
            this.bt1_new = new System.Windows.Forms.Button();
            this.bt2_save = new System.Windows.Forms.Button();
            this.bt3_delete = new System.Windows.Forms.Button();
            this.tB1_ean = new System.Windows.Forms.TextBox();
            this.tB3_count = new System.Windows.Forms.TextBox();
            this.nuD1_price = new System.Windows.Forms.NumericUpDown();
            this.dTP1_inStore = new System.Windows.Forms.DateTimePicker();
            this.lb7_artCount = new System.Windows.Forms.Label();
            this.tb2_desc = new System.Windows.Forms.TextBox();
            this.lb8_category = new System.Windows.Forms.Label();
            this.cB1_category = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.nuD1_price)).BeginInit();
            this.SuspendLayout();
            // 
            // lb1_artVerv
            // 
            this.lb1_artVerv.AutoSize = true;
            this.lb1_artVerv.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb1_artVerv.Location = new System.Drawing.Point(27, 38);
            this.lb1_artVerv.Name = "lb1_artVerv";
            this.lb1_artVerv.Size = new System.Drawing.Size(152, 24);
            this.lb1_artVerv.TabIndex = 0;
            this.lb1_artVerv.Text = "Artikelverwaltung";
            // 
            // liBo1_artList
            // 
            this.liBo1_artList.FormattingEnabled = true;
            this.liBo1_artList.Location = new System.Drawing.Point(14, 64);
            this.liBo1_artList.Name = "liBo1_artList";
            this.liBo1_artList.Size = new System.Drawing.Size(165, 329);
            this.liBo1_artList.TabIndex = 1;
            this.liBo1_artList.SelectedIndexChanged += new System.EventHandler(this.liBo1_artList_SelectedIndexChanged);
            // 
            // lb2_ean
            // 
            this.lb2_ean.AutoSize = true;
            this.lb2_ean.Location = new System.Drawing.Point(221, 72);
            this.lb2_ean.Name = "lb2_ean";
            this.lb2_ean.Size = new System.Drawing.Size(32, 13);
            this.lb2_ean.TabIndex = 2;
            this.lb2_ean.Text = "EAN:";
            // 
            // lb3_bezeichnung
            // 
            this.lb3_bezeichnung.AutoSize = true;
            this.lb3_bezeichnung.Location = new System.Drawing.Point(221, 98);
            this.lb3_bezeichnung.Name = "lb3_bezeichnung";
            this.lb3_bezeichnung.Size = new System.Drawing.Size(59, 13);
            this.lb3_bezeichnung.TabIndex = 3;
            this.lb3_bezeichnung.Text = "Artikelbez.:";
            // 
            // lb4_vkp
            // 
            this.lb4_vkp.AutoSize = true;
            this.lb4_vkp.Location = new System.Drawing.Point(221, 253);
            this.lb4_vkp.Name = "lb4_vkp";
            this.lb4_vkp.Size = new System.Drawing.Size(74, 13);
            this.lb4_vkp.TabIndex = 4;
            this.lb4_vkp.Text = "Verkaufspreis:";
            // 
            // lb5_sortiment
            // 
            this.lb5_sortiment.AutoSize = true;
            this.lb5_sortiment.Location = new System.Drawing.Point(221, 295);
            this.lb5_sortiment.Name = "lb5_sortiment";
            this.lb5_sortiment.Size = new System.Drawing.Size(82, 13);
            this.lb5_sortiment.TabIndex = 5;
            this.lb5_sortiment.Text = "im Sortiment ab:";
            // 
            // lb6_lst
            // 
            this.lb6_lst.AutoSize = true;
            this.lb6_lst.Location = new System.Drawing.Point(221, 337);
            this.lb6_lst.Name = "lb6_lst";
            this.lb6_lst.Size = new System.Drawing.Size(63, 13);
            this.lb6_lst.TabIndex = 6;
            this.lb6_lst.Text = "Lagerstand:";
            // 
            // bt1_new
            // 
            this.bt1_new.Location = new System.Drawing.Point(218, 370);
            this.bt1_new.Name = "bt1_new";
            this.bt1_new.Size = new System.Drawing.Size(75, 23);
            this.bt1_new.TabIndex = 7;
            this.bt1_new.Text = "Neuanlage";
            this.bt1_new.UseVisualStyleBackColor = true;
            this.bt1_new.Click += new System.EventHandler(this.bt1_new_Click);
            // 
            // bt2_save
            // 
            this.bt2_save.Location = new System.Drawing.Point(300, 370);
            this.bt2_save.Name = "bt2_save";
            this.bt2_save.Size = new System.Drawing.Size(75, 23);
            this.bt2_save.TabIndex = 8;
            this.bt2_save.Text = "Speichern";
            this.bt2_save.UseVisualStyleBackColor = true;
            this.bt2_save.Click += new System.EventHandler(this.bt2_save_Click);
            // 
            // bt3_delete
            // 
            this.bt3_delete.Location = new System.Drawing.Point(381, 371);
            this.bt3_delete.Name = "bt3_delete";
            this.bt3_delete.Size = new System.Drawing.Size(75, 23);
            this.bt3_delete.TabIndex = 9;
            this.bt3_delete.Text = "Löschen";
            this.bt3_delete.UseVisualStyleBackColor = true;
            this.bt3_delete.Click += new System.EventHandler(this.bt3_delete_Click);
            // 
            // tB1_ean
            // 
            this.tB1_ean.Location = new System.Drawing.Point(302, 69);
            this.tB1_ean.MaxLength = 13;
            this.tB1_ean.Multiline = true;
            this.tB1_ean.Name = "tB1_ean";
            this.tB1_ean.Size = new System.Drawing.Size(207, 20);
            this.tB1_ean.TabIndex = 10;
            // 
            // tB3_count
            // 
            this.tB3_count.Location = new System.Drawing.Point(303, 334);
            this.tB3_count.Name = "tB3_count";
            this.tB3_count.Size = new System.Drawing.Size(206, 20);
            this.tB3_count.TabIndex = 14;
            // 
            // nuD1_price
            // 
            this.nuD1_price.DecimalPlaces = 2;
            this.nuD1_price.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nuD1_price.Location = new System.Drawing.Point(301, 251);
            this.nuD1_price.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nuD1_price.Name = "nuD1_price";
            this.nuD1_price.Size = new System.Drawing.Size(107, 20);
            this.nuD1_price.TabIndex = 15;
            this.nuD1_price.ThousandsSeparator = true;
            // 
            // dTP1_inStore
            // 
            this.dTP1_inStore.Location = new System.Drawing.Point(309, 289);
            this.dTP1_inStore.Name = "dTP1_inStore";
            this.dTP1_inStore.Size = new System.Drawing.Size(200, 20);
            this.dTP1_inStore.TabIndex = 17;
            // 
            // lb7_artCount
            // 
            this.lb7_artCount.AutoSize = true;
            this.lb7_artCount.Location = new System.Drawing.Point(14, 400);
            this.lb7_artCount.Name = "lb7_artCount";
            this.lb7_artCount.Size = new System.Drawing.Size(0, 13);
            this.lb7_artCount.TabIndex = 18;
            // 
            // tb2_desc
            // 
            this.tb2_desc.Location = new System.Drawing.Point(302, 95);
            this.tb2_desc.MaxLength = 150;
            this.tb2_desc.Multiline = true;
            this.tb2_desc.Name = "tb2_desc";
            this.tb2_desc.Size = new System.Drawing.Size(207, 104);
            this.tb2_desc.TabIndex = 19;
            this.tb2_desc.UseSystemPasswordChar = true;
            // 
            // lb8_category
            // 
            this.lb8_category.AutoSize = true;
            this.lb8_category.Location = new System.Drawing.Point(224, 215);
            this.lb8_category.Name = "lb8_category";
            this.lb8_category.Size = new System.Drawing.Size(58, 13);
            this.lb8_category.TabIndex = 20;
            this.lb8_category.Text = "Kategorie: ";
            // 
            // cB1_category
            // 
            this.cB1_category.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cB1_category.FormattingEnabled = true;
            this.cB1_category.Location = new System.Drawing.Point(309, 215);
            this.cB1_category.Name = "cB1_category";
            this.cB1_category.Size = new System.Drawing.Size(200, 21);
            this.cB1_category.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 426);
            this.Controls.Add(this.cB1_category);
            this.Controls.Add(this.lb8_category);
            this.Controls.Add(this.tb2_desc);
            this.Controls.Add(this.lb7_artCount);
            this.Controls.Add(this.dTP1_inStore);
            this.Controls.Add(this.nuD1_price);
            this.Controls.Add(this.tB3_count);
            this.Controls.Add(this.tB1_ean);
            this.Controls.Add(this.bt3_delete);
            this.Controls.Add(this.bt2_save);
            this.Controls.Add(this.bt1_new);
            this.Controls.Add(this.lb6_lst);
            this.Controls.Add(this.lb5_sortiment);
            this.Controls.Add(this.lb4_vkp);
            this.Controls.Add(this.lb3_bezeichnung);
            this.Controls.Add(this.lb2_ean);
            this.Controls.Add(this.liBo1_artList);
            this.Controls.Add(this.lb1_artVerv);
            this.Name = "Form1";
            this.Text = "Artikelverwaltung";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nuD1_price)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb1_artVerv;
        private System.Windows.Forms.ListBox liBo1_artList;
        private System.Windows.Forms.Label lb2_ean;
        private System.Windows.Forms.Label lb3_bezeichnung;
        private System.Windows.Forms.Label lb4_vkp;
        private System.Windows.Forms.Label lb5_sortiment;
        private System.Windows.Forms.Label lb6_lst;
        private System.Windows.Forms.Button bt1_new;
        private System.Windows.Forms.Button bt2_save;
        private System.Windows.Forms.Button bt3_delete;
        private System.Windows.Forms.TextBox tB1_ean;
        private System.Windows.Forms.TextBox tB3_count;
        private System.Windows.Forms.NumericUpDown nuD1_price;
        private System.Windows.Forms.DateTimePicker dTP1_inStore;
        private System.Windows.Forms.Label lb7_artCount;
        private System.Windows.Forms.TextBox tb2_desc;
        private System.Windows.Forms.Label lb8_category;
        private System.Windows.Forms.ComboBox cB1_category;
    }
}

