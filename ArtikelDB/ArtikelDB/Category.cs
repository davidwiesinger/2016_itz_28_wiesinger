﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtikelDB
{
    class Category
    {

        int id;
        int discount;
        string description;

        public Category(int id, string desc, int discount )
        {
            ID = id;
            Discount = discount;
            Description = desc;
        }

        public override string ToString()
        {
            return Description;
        }

        public int ID
        {
            get { return id; }
            set {
                id = value;
            }
        }

        public int Discount
        {
            get { return discount; }
            set
            {
                discount = value;
            }
        }

        public string Description
        {
            get { return description; }
            set
            {
                description = value;
            }
        }

    }
}
