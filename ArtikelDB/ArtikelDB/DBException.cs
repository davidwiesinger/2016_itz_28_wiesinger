﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ArtikelDB
{


    public class DBException : Exception
    {
        public  DBException() { }
        public  DBException(string message) : base(message) { }
        public  DBException(string message, Exception inner) : base(message, inner)
        {
            Write(inner);
        }

        private void Write(Exception inner)
        {
            StreamWriter sw = new StreamWriter("errors.log",true);
            sw.WriteLine(inner.Message);
            sw.Flush();
            sw.Close();
        }

    }
}
