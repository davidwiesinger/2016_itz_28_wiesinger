﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace ArtikelDB
{
    public partial class Form1 : Form
    { 
        Article actArt = null;

        public Form1()
        {
            InitializeComponent();
            DisableControls();
            bt3_delete.Enabled = false;
            try
            {
                DatabaseConnection.GetInstance().Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message, "Fehler!");
                Environment.Exit(404);
            }
            FillListBox();
        }

        private void FillListBox()
        {
            liBo1_artList.Items.Clear();
            OleDbDataReader reader = null;
            int count = 0;
            string sql = "SELECT ID,Bezeichnung FROM Artikel";
            string countstr = "SELECT COUNT(*) FROM Artikel";
            try
            {
                OleDbCommand cmd = new OleDbCommand(sql, DatabaseConnection.GetInstance().Con);
                OleDbCommand countcmd = new OleDbCommand(countstr, DatabaseConnection.GetInstance().Con);
                count = (int)countcmd.ExecuteScalar();
                reader = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fehler");
                return;
            }
            while (reader.Read())
            {
                int id;
                string desc;
                id = reader.GetInt32(0);
                desc = (reader.GetValue(1) != DBNull.Value) ? reader.GetString(1) : "Keine Beschreibung vorhanden";
                liBo1_artList.Items.Add(new Article(id, desc));
            }

            lb7_artCount.Text = "Anzahl der Artikel: " + count;
        }

        private void FillComoboBox()
        {
            cB1_category.Items.Clear();
            OleDbDataReader reader = null;
            string sql = "SELECT ID, Bez, Rabatt FROM Kategorie";
            try
            {
                OleDbCommand cmd = new OleDbCommand(sql, DatabaseConnection.GetInstance().Con);
                reader = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                DBException dbex = new DBException("Fehler", ex);
                MessageBox.Show(ex.Message, "Fehler!");
            }
            while (reader.Read())
            {
                int id, discount;
                string desc;
                id = reader.GetInt32(0);
                discount = (reader.GetValue(2) != DBNull.Value) ? reader.GetInt32(2) : 0;
                desc = (reader.GetValue(1) != DBNull.Value) ? reader.GetString(1) : "Unbekannte Kategorie";
                cB1_category.Items.Add(new Category(id, desc, discount));
            }


        }

        private void liBo1_artList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (liBo1_artList.SelectedItem != null)
            {
                string sql = "SELECT ID, EAN, Bezeichnung, Lager, ImSortiment, VKP, Kategorie FROM Artikel WHERE ID = " + 
                    ((Article)liBo1_artList.SelectedItem).ID + " ORDER BY Bezeichnung";
                OleDbDataReader reader = null;

                try
                {
                    OleDbCommand cmd = new OleDbCommand(sql, DatabaseConnection.GetInstance().Con);
                    reader = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message, "Fehler");
                }
                while (reader.Read())
                {
                    string s = (reader.GetValue(5) != DBNull.Value) ? Convert.ToString(reader.GetDouble(5)) : "0.0";
                    actArt = new Article(reader.GetInt32(0),                  
                   (reader.GetValue(1) != DBNull.Value) ? reader.GetString(1) : "000000000",
                   (reader.GetValue(2) != DBNull.Value) ? reader.GetString(2) : "No Description",
                   (reader.GetValue(3) != DBNull.Value) ? reader.GetInt32(3) : 0,
                   (reader.GetValue(4) != DBNull.Value) ? reader.GetDateTime(4) : new DateTime(2015, 4, 15),
                   (reader.GetValue(5) != DBNull.Value) ? reader.GetDouble(5) : 0.0,
                   (reader.GetValue(6) != DBNull.Value) ? reader.GetInt32(6) : 1);
                }

                tB1_ean.Text = actArt.EAN;
                dTP1_inStore.Value = actArt.InStore;
                tB3_count.Text = Convert.ToString(actArt.Count);
                tb2_desc.Text = actArt.Description;
                nuD1_price.Value = Convert.ToDecimal(actArt.SalePrice);
                actArt.IsNew = false;
                FillComoboBox();
                cB1_category.SelectedIndex = actArt.Category;
                EnableControls();
                bt3_delete.Enabled = true;
            }
        }

        private void bt1_new_Click(object sender, EventArgs e)
        {
            bt3_delete.Enabled = false;
            EnableControls();
            actArt = new Article();
            actArt.IsNew = true;
            tb2_desc.Clear();
            tB1_ean.Clear();
            tB3_count.Clear();
            nuD1_price.Value = 0;
            dTP1_inStore.Value = DateTime.Now;

        }

        private void bt2_save_Click(object sender, EventArgs e)
        {
           
            int res  = 0;
            int f;
            Int64 f1;
            if (long.TryParse(tB1_ean.Text, out f1) && int.TryParse(tB3_count.Text, out f) && tb2_desc.Text != "" && ((Category)cB1_category.SelectedItem).ID != 0)
            {
                if (MessageBox.Show("Wollen Sie wirklich speichern?", "Achtung", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {                    
                    DisableControls();
                    actArt.EAN = tB1_ean.Text;
                    actArt.Count = Convert.ToInt32(tB3_count.Text);
                    actArt.Description = tb2_desc.Text;
                    actArt.InStore = dTP1_inStore.Value;
                    actArt.SalePrice = Convert.ToDouble(nuD1_price.Value);
                    actArt.Category = ((Category)cB1_category.SelectedItem).ID;
                    try
                    {
                        res = actArt.WriteToDB(DatabaseConnection.GetInstance().Con);
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.InnerException.InnerException.Message, "Error");
                    }
                    if (res == 0)
                    {
                        MessageBox.Show("Kein Datensatz verändert!", "Info");
                    }
                    else if (res > 10 && res < 20)
                    {
                        MessageBox.Show(res - 10 + " Datensätz(e) erfolgreich hinzugefügt!", "Info");
                    }
                    else if (res > 20)
                    {
                        MessageBox.Show(res - 20 + " Datensätz(e) erfolgreich verändert!", "Info");
                    }
                    FillListBox();
                }
                
            }
            else if (!Int64.TryParse(tB1_ean.Text, out f1))
            {
                MessageBox.Show("Bitte eine Zahlenfolge eingeben!", "Fehler");
            }
            else if (!int.TryParse(tB3_count.Text, out f))
            {
                MessageBox.Show("Bitte Anzahl der Artikel auf Lager als Zahl eingeben!", "Fehler");
            }
            else if (tb2_desc.Text == "")
            {
                MessageBox.Show("Beschreibung darf nicht leer sein!", "Fehler");
            }
            else if (((Category)cB1_category.SelectedItem).ID != 0)
            {
                MessageBox.Show("Kategorie darf nicht 0 sein!", "Fehler");
            }
            

           
        }

        private void bt3_delete_Click(object sender, EventArgs e)
        {
            int res = 0;
            if (MessageBox.Show("Wollen Sie das Element wirklich löschen?", "Achtung", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    res = actArt.DeleteFromDB(DatabaseConnection.GetInstance().Con);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.InnerException.InnerException.Message, "Error");
                }
                if (res > 0)
                {
                    MessageBox.Show(res + " Datensätz(e) gelöscht!", "Info");
                }
                else
                {
                    MessageBox.Show("Kein Datensatz verändert!", "Info");
                }
               
                actArt = null;
                FillListBox();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DatabaseConnection.GetInstance().Close();
        }

        private void DisableControls()
        {
            tB1_ean.Enabled = false;
            dTP1_inStore.Enabled = false;
            tB3_count.Enabled = false;
            nuD1_price.Enabled = false;
            tb2_desc.Enabled = false;
        }

        private void EnableControls()
        {

            tB1_ean.Enabled = true;
            dTP1_inStore.Enabled = true;
            tB3_count.Enabled = true;
            nuD1_price.Enabled = true;
            tb2_desc.Enabled = true;
            
        }
    }
}
