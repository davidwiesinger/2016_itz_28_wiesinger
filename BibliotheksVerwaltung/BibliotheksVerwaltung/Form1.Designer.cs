﻿namespace BibliotheksVerwaltung
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.laISBN = new System.Windows.Forms.Label();
            this.bGetByISBN = new System.Windows.Forms.Button();
            this.bGetCheapestBook = new System.Windows.Forms.Button();
            this.laISBNins = new System.Windows.Forms.Label();
            this.laTitelins = new System.Windows.Forms.Label();
            this.tbTitelins = new System.Windows.Forms.TextBox();
            this.laAutorins = new System.Windows.Forms.Label();
            this.laJahrins = new System.Windows.Forms.Label();
            this.tbJahrins = new System.Windows.Forms.TextBox();
            this.laPreisins = new System.Windows.Forms.Label();
            this.tbPreisins = new System.Windows.Forms.TextBox();
            this.bInsert = new System.Windows.Forms.Button();
            this.mtbISBNins = new System.Windows.Forms.MaskedTextBox();
            this.mtbISBN = new System.Windows.Forms.MaskedTextBox();
            this.laThemenIDins = new System.Windows.Forms.Label();
            this.cbThemenIDins = new System.Windows.Forms.ComboBox();
            this.tbAutorins = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // laISBN
            // 
            this.laISBN.AutoSize = true;
            this.laISBN.Location = new System.Drawing.Point(24, 13);
            this.laISBN.Name = "laISBN";
            this.laISBN.Size = new System.Drawing.Size(43, 17);
            this.laISBN.TabIndex = 1;
            this.laISBN.Text = "ISBN:";
            // 
            // bGetByISBN
            // 
            this.bGetByISBN.Location = new System.Drawing.Point(27, 61);
            this.bGetByISBN.Name = "bGetByISBN";
            this.bGetByISBN.Size = new System.Drawing.Size(264, 29);
            this.bGetByISBN.TabIndex = 2;
            this.bGetByISBN.Text = "getByISBN";
            this.bGetByISBN.UseVisualStyleBackColor = true;
            this.bGetByISBN.Click += new System.EventHandler(this.bGetByISBN_Click);
            // 
            // bGetCheapestBook
            // 
            this.bGetCheapestBook.Location = new System.Drawing.Point(27, 111);
            this.bGetCheapestBook.Name = "bGetCheapestBook";
            this.bGetCheapestBook.Size = new System.Drawing.Size(264, 29);
            this.bGetCheapestBook.TabIndex = 3;
            this.bGetCheapestBook.Text = "getCheapestBook";
            this.bGetCheapestBook.UseVisualStyleBackColor = true;
            this.bGetCheapestBook.Click += new System.EventHandler(this.bGetCheapestBook_Click);
            // 
            // laISBNins
            // 
            this.laISBNins.AutoSize = true;
            this.laISBNins.Location = new System.Drawing.Point(24, 208);
            this.laISBNins.Name = "laISBNins";
            this.laISBNins.Size = new System.Drawing.Size(39, 17);
            this.laISBNins.TabIndex = 4;
            this.laISBNins.Text = "ISBN";
            // 
            // laTitelins
            // 
            this.laTitelins.AutoSize = true;
            this.laTitelins.Location = new System.Drawing.Point(24, 261);
            this.laTitelins.Name = "laTitelins";
            this.laTitelins.Size = new System.Drawing.Size(35, 17);
            this.laTitelins.TabIndex = 6;
            this.laTitelins.Text = "Titel";
            // 
            // tbTitelins
            // 
            this.tbTitelins.Location = new System.Drawing.Point(27, 281);
            this.tbTitelins.Name = "tbTitelins";
            this.tbTitelins.Size = new System.Drawing.Size(264, 22);
            this.tbTitelins.TabIndex = 6;
            // 
            // laAutorins
            // 
            this.laAutorins.AutoSize = true;
            this.laAutorins.Location = new System.Drawing.Point(24, 315);
            this.laAutorins.Name = "laAutorins";
            this.laAutorins.Size = new System.Drawing.Size(42, 17);
            this.laAutorins.TabIndex = 8;
            this.laAutorins.Text = "Autor";
            // 
            // laJahrins
            // 
            this.laJahrins.AutoSize = true;
            this.laJahrins.Location = new System.Drawing.Point(24, 365);
            this.laJahrins.Name = "laJahrins";
            this.laJahrins.Size = new System.Drawing.Size(118, 17);
            this.laJahrins.TabIndex = 9;
            this.laJahrins.Text = "Erscheinungsjahr";
            // 
            // tbJahrins
            // 
            this.tbJahrins.Location = new System.Drawing.Point(27, 385);
            this.tbJahrins.Name = "tbJahrins";
            this.tbJahrins.Size = new System.Drawing.Size(264, 22);
            this.tbJahrins.TabIndex = 8;
            // 
            // laPreisins
            // 
            this.laPreisins.AutoSize = true;
            this.laPreisins.Location = new System.Drawing.Point(24, 417);
            this.laPreisins.Name = "laPreisins";
            this.laPreisins.Size = new System.Drawing.Size(40, 17);
            this.laPreisins.TabIndex = 12;
            this.laPreisins.Text = "Preis";
            // 
            // tbPreisins
            // 
            this.tbPreisins.Location = new System.Drawing.Point(27, 437);
            this.tbPreisins.Name = "tbPreisins";
            this.tbPreisins.Size = new System.Drawing.Size(264, 22);
            this.tbPreisins.TabIndex = 9;
            // 
            // bInsert
            // 
            this.bInsert.Location = new System.Drawing.Point(27, 474);
            this.bInsert.Name = "bInsert";
            this.bInsert.Size = new System.Drawing.Size(264, 29);
            this.bInsert.TabIndex = 10;
            this.bInsert.Text = "Buch hinzufügen";
            this.bInsert.UseVisualStyleBackColor = true;
            this.bInsert.Click += new System.EventHandler(this.bInsert_Click);
            // 
            // mtbISBNins
            // 
            this.mtbISBNins.AsciiOnly = true;
            this.mtbISBNins.Location = new System.Drawing.Point(27, 228);
            this.mtbISBNins.Mask = "0-000000-00-0";
            this.mtbISBNins.Name = "mtbISBNins";
            this.mtbISBNins.Size = new System.Drawing.Size(264, 22);
            this.mtbISBNins.TabIndex = 5;
            // 
            // mtbISBN
            // 
            this.mtbISBN.AsciiOnly = true;
            this.mtbISBN.Location = new System.Drawing.Point(27, 33);
            this.mtbISBN.Mask = "0-000000-00-0";
            this.mtbISBN.Name = "mtbISBN";
            this.mtbISBN.Size = new System.Drawing.Size(264, 22);
            this.mtbISBN.TabIndex = 1;
            // 
            // laThemenIDins
            // 
            this.laThemenIDins.AutoSize = true;
            this.laThemenIDins.Location = new System.Drawing.Point(24, 157);
            this.laThemenIDins.Name = "laThemenIDins";
            this.laThemenIDins.Size = new System.Drawing.Size(78, 17);
            this.laThemenIDins.TabIndex = 15;
            this.laThemenIDins.Text = "Themen-ID";
            // 
            // cbThemenIDins
            // 
            this.cbThemenIDins.FormattingEnabled = true;
            this.cbThemenIDins.Location = new System.Drawing.Point(27, 177);
            this.cbThemenIDins.Name = "cbThemenIDins";
            this.cbThemenIDins.Size = new System.Drawing.Size(264, 24);
            this.cbThemenIDins.TabIndex = 4;
            // 
            // tbAutorins
            // 
            this.tbAutorins.Location = new System.Drawing.Point(27, 335);
            this.tbAutorins.Name = "tbAutorins";
            this.tbAutorins.Size = new System.Drawing.Size(264, 22);
            this.tbAutorins.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 521);
            this.Controls.Add(this.cbThemenIDins);
            this.Controls.Add(this.mtbISBN);
            this.Controls.Add(this.mtbISBNins);
            this.Controls.Add(this.laThemenIDins);
            this.Controls.Add(this.bInsert);
            this.Controls.Add(this.tbPreisins);
            this.Controls.Add(this.laPreisins);
            this.Controls.Add(this.tbJahrins);
            this.Controls.Add(this.tbAutorins);
            this.Controls.Add(this.laJahrins);
            this.Controls.Add(this.laAutorins);
            this.Controls.Add(this.tbTitelins);
            this.Controls.Add(this.laTitelins);
            this.Controls.Add(this.laISBNins);
            this.Controls.Add(this.bGetCheapestBook);
            this.Controls.Add(this.bGetByISBN);
            this.Controls.Add(this.laISBN);
            this.Name = "Form1";
            this.Text = "Verwaltung";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laISBN;
        private System.Windows.Forms.Button bGetByISBN;
        private System.Windows.Forms.Button bGetCheapestBook;
        private System.Windows.Forms.Label laISBNins;
        private System.Windows.Forms.Label laTitelins;
        private System.Windows.Forms.TextBox tbTitelins;
        private System.Windows.Forms.Label laAutorins;
        private System.Windows.Forms.Label laJahrins;
        private System.Windows.Forms.TextBox tbJahrins;
        private System.Windows.Forms.Label laPreisins;
        private System.Windows.Forms.TextBox tbPreisins;
        private System.Windows.Forms.Button bInsert;
        private System.Windows.Forms.MaskedTextBox mtbISBNins;
        private System.Windows.Forms.MaskedTextBox mtbISBN;
        private System.Windows.Forms.Label laThemenIDins;
        private System.Windows.Forms.ComboBox cbThemenIDins;
        private System.Windows.Forms.TextBox tbAutorins;
    }
}

