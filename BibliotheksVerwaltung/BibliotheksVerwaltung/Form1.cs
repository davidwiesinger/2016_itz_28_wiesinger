﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace BibliotheksVerwaltung
{
    public partial class Form1 : Form
    {
        OleDbConnection conn = null;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            conn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb");
            conn.Open();
            FillcbThemenIDins();
        }

        public void FillcbThemenIDins()
        {
            string sql = "select ID, Themenbereich from Themengruppe";
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cbThemenIDins.Items.Add(reader.GetInt32(0) + " - " + reader.GetString(1));
            }
            reader.Close();
            cbThemenIDins.Sorted = true;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            conn.Close();
        }
        private void bGetByISBN_Click(object sender, EventArgs e)
        {
            string sql = "select Autor from Buch where ISBN='" + mtbISBN.Text + "'";
            mtbISBN.Clear();
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            MessageBox.Show((string)cmd.ExecuteScalar(), "Autor");
        }
        private void bGetCheapestBook_Click(object sender, EventArgs e)
        {
            string sql = "select BuchNr, ThemenID, ISBN, Titel, Autor, Erscheinungsjahr, Preis from Buch where Preis=(select MIN(Preis) from Buch)";
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int themenID = reader.GetInt32(1);
                string ISBN = reader.GetString(2);
                string titel = reader.GetString(3);
                string autor = reader.GetString(4);
                int jahr = reader.GetInt32(5);
                string preis = reader.GetFloat(6).ToString("n2");
                MessageBox.Show("Themen-ID: " + themenID + "\nISBN: " + ISBN + "\nTitel: " + titel + "\nAutor: " + autor + "\nErscheinungsjahr: " + jahr + "\nPreis: " + preis, "Billigstes Buch");
            }
            reader.Close();
        }
        private void bInsert_Click(object sender, EventArgs e)
        {
            int themenID = Convert.ToInt32(cbThemenIDins.SelectedItem.ToString().Substring(0,1));
            string ISBN = mtbISBNins.Text.ToString();
            string Titel = tbTitelins.Text.ToString();
            string Autor = tbAutorins.Text.ToString();
            int Jahr = Convert.ToInt32(tbJahrins.Text);
            float Preis = Convert.ToSingle(tbPreisins.Text);
            cbThemenIDins.ResetText();
            mtbISBNins.Clear();
            tbTitelins.Clear();
            tbAutorins.Clear();
            tbJahrins.Clear();
            tbPreisins.Clear();
            string sql = "insert into Buch (ThemenID, [ISBN], [Titel], [Autor], Erscheinungsjahr, Preis) values (" + themenID + ", '" + ISBN + "', '" + Titel + "', '" + Autor + "', " + Jahr + ", '" + Preis + "')";
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Themen-ID: " + themenID + "\nISBN: " + ISBN + "\nTitel: " + Titel + "\nAutor: " + Autor + "\nErscheinungsjahr: " + Jahr + "\nPreis: " + Preis, "Buch wurde hinzugefügt!");
        }
    }
}
